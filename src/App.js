import React from 'react';
import './App.css';

import { Route, Switch } from 'react-router-dom';
import { useEffect } from "react";
import { useDispatch } from "react-redux";
import { Navigation } from './components/Nav';
import { Login } from "./pages/Login";
import { loginWithToken } from './app/authSlice';
import { Container } from 'react-bootstrap';
import { Home } from './pages/Home';
import { ArticleAdd } from './pages/ArticleAdd';
import { ProtectedRoute } from './components/ProtectedRoute';





function App() {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(loginWithToken());
  }, [dispatch]);


  return (
    <Container id="root">

      <Navigation />

      <Switch>

        <ProtectedRoute path="/article">
          <ArticleAdd/>
        </ProtectedRoute>

        <Route  path="/" exact>
          <Home />
        </Route>

        <Route path="/login">
          <Login />
        </Route>

      </Switch>

    </Container>

  );

}

export default App;

