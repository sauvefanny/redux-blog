import { Card, Tab, Tabs } from "react-bootstrap";
import { LoginForm } from "../components/LoginForm";
import { RegisterForm } from "../components/RegisterForm";


export function Login() {

    return (
        <Tabs defaultActiveKey="se connecter" id="uncontrolled-tab-example" className="mb-3">

            <Tab eventKey="se connecter" title="Se connecter">
                <Card>
                    <LoginForm />
                </Card>
            </Tab >
            <Tab eventKey="Créer un compte" title="Créer un compte">
                <Card>
                    <RegisterForm />
                </Card>
            </Tab >

        </Tabs>
    )
}