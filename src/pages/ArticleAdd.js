import { useState } from "react";
import { Button, Form } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import { Redirect } from "react-router-dom";
import {  newArticle } from "../app/articleSlice";
import { useForm } from "react-hook-form";


export function ArticleAdd() {

    const dispatch = useDispatch();

    const { register, handleSubmit } = useForm();

    const feedback = useSelector(state => state.articles.addArticle);

    const [redirect, setRedirect] = useState(false);

    const onSubmit = (values) => {
        dispatch(newArticle(values));
        console.log(values)
        setRedirect(true)
    }

    return (

        <>
            {redirect ? (<Redirect push to='/home' />) : null}

            <Form onSubmit={handleSubmit(onSubmit)} name="basic">
                {feedback && <p>{feedback}</p>}

                <Form.Group className="mb-3" controlId="formBasicEmail">
                    <Form.Label>image</Form.Label>
                    <Form.Control type="texte" placeholder="lien jpeg de la photo" {...register('img')} />
                </Form.Group>

                <Form.Group>
                    <Form.Label>titre</Form.Label>
                    <Form.Control type="titre" placeholder="titre de l'article" {...register('titre')} />
                </Form.Group>

                <Form.Group className="mb-3" controlId="formBasicPassword">
                    <Form.Label>Contenu</Form.Label>
                    <Form.Control type="contenu" placeholder="contenu" {...register('contenu')} />
                </Form.Group>

                <Form.Group>
                    <Form.Label>date</Form.Label>
                    <Form.Control type="date" placeholder="date" {...register('date')} />
                </Form.Group>

                <Button
                    variant="primary" type="submit"> Envoyer
                </Button>

            </Form>
        </>

    )

}