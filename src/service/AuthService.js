import axios from "axios";

export class AuthService {


    static async register(user) {
        const response = await axios.post(process.env.REACT_APP_SERVER_URL+'/api/user/', user);

        return response.data;
    }


    static async login(credentials) {
        const response = await axios.post(process.env.REACT_APP_SERVER_URL+'/api/user/login', credentials);
        localStorage.setItem('token', response.data.token);
        
        return response.data.user;
    }

    static async fetchAccount(token) {
        const response = await axios.get(process.env.REACT_APP_SERVER_URL+'/api/user/account',
         {headers:{
             "Authorization": `Bearer ${token}`
         }});
        
        return response.data;
    }

}