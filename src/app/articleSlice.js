import { createSlice } from '@reduxjs/toolkit';
import axios from 'axios';

const initialState = {
    list: [],
    selected: []
};

const articleSlice = createSlice({
    name: 'articles',
    initialState,
    reducers: {
        setArticle(state, { payload }) {
            state.list = payload;
        },
        addArticle(state, { payload }) {
            state.list.push(payload);
        },
        // toggleSelectArticle(state, { payload }) {

        //     let index = state.selected.findIndex(item => item.id === payload.id)

        //     if (index !== -1) {

        //         state.selected.splice(index, 1)

        //     } else {
        //         state.selected.push(payload)
        //     }
        // },
        clearSelectedArticle(state) {
            state.selected = []
        }

    }
});

export const { setArticle, addArticle,deleteArticle,  toggleSelectArticle, clearSelectedArticle } = articleSlice.actions;

export default articleSlice.reducer;

/**
 * thunk
 */
export const fetchArticle = () => async (dispatch) => {
    try {
        const response = await axios.get(process.env.REACT_APP_SERVER_URL+'/api/blog/article');
        dispatch(setArticle(response.data));

    } catch (error) {
        console.log(error);
    }
};

export const newArticle = (item) => async (dispatch) => {
    try {
        const resp = await axios.post(process.env.REACT_APP_SERVER_URL+'/api/blog/article', item);
        dispatch(addArticle(resp.data))

    } catch (error) {
        console.log(error)
    }
}
export const DeleteArt= (id) => async (dispatch)=>{
        try {
            await
            axios.delete(process.env.REACT_APP_SERVER_URL+'/api/blog/article' +id)
            
        } catch (error) {
           console.log(error) 
        }
    }

