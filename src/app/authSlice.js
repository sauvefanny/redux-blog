import { createSlice } from "@reduxjs/toolkit";
import { AuthService } from "../service/AuthService";
import { addUser } from "./userSlice";


const authSlice = createSlice({
    name: 'auth',
    initialState: {
        user: null,
        loginFeedback: '',
        registerFeedback: ''
    },
    reducers: {
        login(state, {payload}) {
            state.user = payload;
        },
        logout(state) {
            state.user = false;
            localStorage.removeItem('token');
        },
        updateLoginFeedback(state, {payload}) {
            state.loginFeedback = payload
        },
        updateRegisterFeedback(state, {payload}) {
            state.registerFeedback = payload
        }
    }
});
/**
 * On exporte les différentes actions afin de pouvoir s'en servir
 * facilement depuis les components
 */
export const {login,logout, updateLoginFeedback, updateRegisterFeedback} = authSlice.actions;

//On export le reducer pour le charger dans le store
export default authSlice.reducer;


export const loginWithToken = () => async (dispatch) => {
    const token = localStorage.getItem('token');

    if(token) {
        try {

            const user = await AuthService.fetchAccount(token);

            dispatch(login(user));
        } catch(error) {
            dispatch(logout());
        }
    }
    
}

export const register = (user) => async (dispatch) => {
    try {
        await AuthService.register(user);
        dispatch(updateRegisterFeedback('Registration successful'));
        dispatch(addUser(user));
    } catch (error) {
        dispatch(updateRegisterFeedback(error.response.data.error));
        
    }
}

export const loginWithCredentials = (credentials) => async (dispatch) => {
    try {
        const user = await AuthService.login(credentials);
        dispatch(updateLoginFeedback('Login successful'));
        dispatch(login(user));

    } catch (error) {
        dispatch(updateLoginFeedback('Email and/or password incorrect'));
    }
}