import { configureStore } from '@reduxjs/toolkit';
import articleSlice from './articleSlice';
import authSlice from './authSlice';
import userSlice from './userSlice';


export const store = configureStore({
  reducer: {
    articles : articleSlice,
    user : userSlice,
    auth: authSlice
  },
});
