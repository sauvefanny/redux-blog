import { createSlice } from "@reduxjs/toolkit";


const userSlice = createSlice({
    name: 'user',
    initialState: {
        list: []
    },
    reducers: {
        setList(state, { payload }) {
            state.list = payload;
        },
        addUser(state, { payload }) {
            state.list.push(payload);
        }
    }
});
export const { setList, addUser } = userSlice.actions;

export default userSlice.reducer;

// export const fetchUsers = () => async (dispatch) => {
//     try {
//         const response = await axios.get('http://localhost:8000/api/blog/user');

//         dispatch(setList(response.data));

//     } catch (error) {
//         console.log(error)
//     }
// };