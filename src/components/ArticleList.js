import { useEffect } from "react";

import { useDispatch, useSelector } from "react-redux";
import { Article } from "./Article";

import Row from "react-bootstrap/esm/Row";
import Col from "react-bootstrap/esm/Col";
import { fetchArticle } from "../app/articleSlice";


export function ListArticle() {

    const dispatch = useDispatch()
    const articles = useSelector(state => state.articles.list);

    useEffect(() => {

        dispatch(fetchArticle());
    }, [dispatch]);

    return (


        <Row xs={1} md={2} className="m-4">
            {articles.map(item =>
                <Col key={item.id}>
                    <Article hoverable article={item} />
                </Col>
            )}
        </Row>


    )
}