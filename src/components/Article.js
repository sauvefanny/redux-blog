import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';

import { useDispatch, useSelector } from 'react-redux';
import 'bootstrap/dist/css/bootstrap.min.css';
import moment from 'moment';
import 'moment/locale/fr';
import { DeleteArt, setArticle } from '../app/articleSlice';

moment.locale('fr');


export function Article({ article }) {

    const dispatch = useDispatch();
    const list = useSelector(state => state.articles.list)

    function OnDelete(id) {
        dispatch(setArticle(list.filter(item => item.id !== id)))
        dispatch(DeleteArt(id))
    }


    return (

        <Card  >
            <Card.Img variant="top" src={article.img} className="image_card p-4" />
            <Card.Body>
                <Card.Title className="title_card">{article.titre}</Card.Title>

                <Card.Text>
                    {article.contenu}
                </Card.Text>
                <Button onClick={() => OnDelete(article.id)} variant="primary" > X
                </Button>
                <Card.Footer>
                    <small className="text-muted">{moment(article.date).format('LL')}</small>
                </Card.Footer>
            </Card.Body>
        </Card>

    )
}