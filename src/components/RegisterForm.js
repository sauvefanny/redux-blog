import { useDispatch, useSelector } from "react-redux";
import React, { useState } from "react";
import { useForm } from "react-hook-form";
import { register as registerUser } from '../app/authSlice';
import { Button, Form } from "react-bootstrap";
import { Redirect } from 'react-router-dom';

export function RegisterForm() {

    const { register, handleSubmit } = useForm();
    const [redirect, setRedirect] = useState(false);
    const dispatch = useDispatch();

    const feedback = useSelector(state => state.auth.registerFeedback);

    const onSubmit = (values) => {
        
        setRedirect(true)
        dispatch(registerUser(values));
    }

    return (
        <>
            {redirect ? (<Redirect push to='/login' />) : null}

            <Form onSubmit={handleSubmit(onSubmit)} name="basic">
                {feedback && <p>{feedback}</p>}

                <Form.Group className="mb-3" controlId="form">
                    <Form.Label>Pseudo</Form.Label>
                    <Form.Control type="pseudo" placeholder="pseudo" {...register('pseudo')} />
                </Form.Group>

                <Form.Group className="mb-3" controlId="formBasicEmail">
                    <Form.Label>Email</Form.Label>

                    <Form.Control type="email" placeholder="Enter email" {...register('email')} />

                    <Form.Text className="text-muted">
                        votre adresse mail ne sera pas partager
                    </Form.Text>

                </Form.Group>

                <Form.Group className="mb-3" controlId="formBasicPassword">
                    <Form.Label>Mot de passe</Form.Label>
                    <Form.Control type="mdp" placeholder="Mot de passe" {...register('mdp')} />
                </Form.Group>

                <Button variant="primary" type="submit">
                    Envoyer
                </Button>

            </Form>
        </>
    )
}