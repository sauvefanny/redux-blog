import { Form, Button } from "react-bootstrap";
import { useDispatch, useSelector } from 'react-redux';
import React, { useState } from "react";
import { useForm } from "react-hook-form";
import { loginWithCredentials } from '../app/authSlice';
import { Redirect } from 'react-router-dom';

export function LoginForm() {

    const { register, handleSubmit } = useForm();

    const [redirect, setRedirect] = useState(false);

    const dispatch = useDispatch();

    const feedback = useSelector(state => state.auth.loginFeedback);

    const onSubmit = (values) => {
        dispatch(loginWithCredentials(values));
        setRedirect(true)
    }

    return (
        <>
            {redirect ? (<Redirect push to='/' />) : null}


            <Form onSubmit={handleSubmit(onSubmit)} name="basic">
                {feedback && <p>{feedback}</p>}
                <Form.Group className="mb-3" controlId="formBasicEmail">
                    <Form.Label>Email</Form.Label>

                    <Form.Control type="email" placeholder="Enter email" {...register('email')} />

                    <Form.Text className="text-muted">
                        votre adresse mail ne sera pas partager
                    </Form.Text>

                </Form.Group>

                <Form.Group className="mb-3" controlId="formBasicPassword">
                    <Form.Label>Mot de passe</Form.Label>
                    <Form.Control type="mdp" placeholder="Mot de passe" {...register('mdp')} />
                </Form.Group>
                <Form.Group className="mb-3" controlId="formBasicCheckbox">
                    <Form.Check type="checkbox" label="Check me out" />
                </Form.Group>


                <Button

                    variant="primary" type="submit"> Envoyer
                </Button>

            </Form >

        </>
    )
}