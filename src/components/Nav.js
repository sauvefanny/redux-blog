
import React from 'react';
import { Button} from 'react-bootstrap';
import Nav from 'react-bootstrap/Nav';
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { logout } from '../app/authSlice';
import "./Nav.css";



export function Navigation() {


    const dispatch = useDispatch();
    const user = useSelector(state => state.auth.user);


    return (
      <>      
                <Nav className="justify-content-center" >
                    <Link to="/" className="titre">
                        <p>Pépinière <br />
                            <span className="span">d'un Songe
                            </span>
                        </p>
                    </Link>
                </Nav>

                <Nav style={{ textDecoration: 'none' }} className="lienNav justify-content-center" activeKey="/">

                    <Nav.Item>
                        <Link className="link" style={{ color: 'inherit', textDecoration: 'inherit' }} to="/">Accueil</Link>
                    </Nav.Item>

                    {user ? <>
                        <Nav.Item>
                            <Link className="link" style={{ color: 'inherit', textDecoration: 'inherit' }} to="/article" >Nouvel Article</Link>
                        </Nav.Item>

                        <Nav.Item>
                            <Button variant="light" onClick={() => dispatch(logout())}>
                                Se déconnecter
                            </Button>
                        </Nav.Item>
                    </>
                        :
                        <Nav.Item>
                            <Link className="link" style={{ color: 'inherit', textDecoration: 'inherit' }} to="/login">
                                Se connecter/Créer un compte
                            </Link>
                        </Nav.Item>}

                </Nav>
</>

    )
}
